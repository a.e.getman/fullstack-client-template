import 'package:common/common.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    Dummy dummy;

    setUp(() {
      dummy = Dummy.builder((b) => b..name = 'test');
    });

    test('First Test', () {
      expect(dummy.name, 'test');
    });
  });
}
