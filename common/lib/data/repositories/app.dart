import 'package:common/models/models.dart';

abstract class AppRepository {
  Future<List<Dummy>> loadData();
}
