import 'package:built_redux/built_redux.dart';
import 'package:common/models/models.dart';

part 'app.g.dart';

abstract class AppActions extends ReduxActions {
  AppActions._();

  factory AppActions() => new _$AppActions();

  ActionDispatcher<Null> get loadData;
  ActionDispatcher<List<Dummy>> get loadDataSuccess;
  ActionDispatcher<Null> get loadDataFailed;
  ActionDispatcher<Null> get increment;
}
