// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app.dart';

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$AppActions extends AppActions {
  factory _$AppActions() => new _$AppActions._();
  _$AppActions._() : super._();

  final ActionDispatcher<Null> loadData =
      new ActionDispatcher<Null>('AppActions-loadData');
  final ActionDispatcher<List<Dummy>> loadDataSuccess =
      new ActionDispatcher<List<Dummy>>('AppActions-loadDataSuccess');
  final ActionDispatcher<Null> loadDataFailed =
      new ActionDispatcher<Null>('AppActions-loadDataFailed');
  final ActionDispatcher<Null> increment =
      new ActionDispatcher<Null>('AppActions-increment');

  @override
  void setDispatcher(Dispatcher dispatcher) {
    loadData.setDispatcher(dispatcher);
    loadDataSuccess.setDispatcher(dispatcher);
    loadDataFailed.setDispatcher(dispatcher);
    increment.setDispatcher(dispatcher);
  }
}

class AppActionsNames {
  static final ActionName<Null> loadData =
      new ActionName<Null>('AppActions-loadData');
  static final ActionName<List<Dummy>> loadDataSuccess =
      new ActionName<List<Dummy>>('AppActions-loadDataSuccess');
  static final ActionName<Null> loadDataFailed =
      new ActionName<Null>('AppActions-loadDataFailed');
  static final ActionName<Null> increment =
      new ActionName<Null>('AppActions-increment');
}
