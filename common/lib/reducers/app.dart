import 'package:built_redux/built_redux.dart';
import 'package:common/actions/actions.dart';
import 'package:common/models/models.dart';

var appReducerBuilder = ReducerBuilder<AppState, AppStateBuilder>()
  ..add(AppActionsNames.loadDataSuccess, _loadDataSuccess)
  ..add(AppActionsNames.loadDataFailed, _loadDataFailed)
  ..add(AppActionsNames.increment, _increment);

void _loadDataSuccess(
    AppState state, Action<List<Dummy>> action, AppStateBuilder builder) {
  builder..dummyList = action.payload;
}

void _loadDataFailed(
    AppState state, Action<Null> action, AppStateBuilder builder) {
  builder..dummyList.clear();
}

void _increment(AppState state, Action<Null> action, AppStateBuilder builder) {
  builder..counter = state.counter + 1;
}
