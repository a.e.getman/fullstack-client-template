// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dummy.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<Dummy> _$dummySerializer = new _$DummySerializer();

class _$DummySerializer implements StructuredSerializer<Dummy> {
  @override
  final Iterable<Type> types = const [Dummy, _$Dummy];
  @override
  final String wireName = 'Dummy';

  @override
  Iterable serialize(Serializers serializers, Dummy object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
    ];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(int)));
    }

    return result;
  }

  @override
  Dummy deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DummyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$Dummy extends Dummy {
  @override
  final int id;
  @override
  final String name;

  factory _$Dummy([void updates(DummyBuilder b)]) =>
      (new DummyBuilder()..update(updates)).build();

  _$Dummy._({this.id, this.name}) : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('Dummy', 'name');
    }
  }

  @override
  Dummy rebuild(void updates(DummyBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  DummyBuilder toBuilder() => new DummyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Dummy && id == other.id && name == other.name;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), name.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Dummy')
          ..add('id', id)
          ..add('name', name))
        .toString();
  }
}

class DummyBuilder implements Builder<Dummy, DummyBuilder> {
  _$Dummy _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  DummyBuilder();

  DummyBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Dummy other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Dummy;
  }

  @override
  void update(void updates(DummyBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Dummy build() {
    final _$result = _$v ?? new _$Dummy._(id: id, name: name);
    replace(_$result);
    return _$result;
  }
}
