import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'dummy.g.dart';

abstract class Dummy implements Built<Dummy, DummyBuilder> {
  static Serializer<Dummy> get serializer => _$dummySerializer;

  Dummy._();

  factory Dummy.builder([updates(DummyBuilder b)]) {
    final builder = DummyBuilder()
      ..name = ''
      ..update(updates);
    return builder.build();
  }

  factory Dummy([updates(DummyBuilder b)]) => _$Dummy((b) => b..name = '');

  @nullable
  int get id;

  String get name;
}
