import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:common/models/dummy/dummy.dart';

part 'app.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  static Serializer<AppState> get serializer => _$appStateSerializer;

  AppState._();

  factory AppState([updates(AppStateBuilder b)]) => _$AppState((b) => b
    ..dummyList = []
    ..counter = 0);

  List<Dummy> get dummyList;

  int get counter;
}
