import 'package:built_redux/built_redux.dart';
import 'package:common/actions/actions.dart';
import 'package:common/data/repositories/repositories.dart';
import 'package:common/models/models.dart';

Middleware<AppState, AppStateBuilder, AppActions> createAppMiddleware(
  AppRepository appRepository,
) {
  return (MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
        ..add(AppActionsNames.loadData, _createLoadData(appRepository)))
      .build();
}

MiddlewareHandler<AppState, AppStateBuilder, AppActions, Null> _createLoadData(
    AppRepository repository) {
  return (MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
      ActionHandler next, Action<Null> action) {
    repository
        .loadData()
        .then((data) => api.actions.loadDataSuccess(data))
        .catchError(api.actions.loadDataFailed);

    next(action);
  };
}
