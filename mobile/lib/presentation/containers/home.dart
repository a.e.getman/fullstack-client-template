import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'package:mobile/presentation/widgets/widgets.dart';

class HomePage extends StoreConnector<AppState, AppActions, int> {
  @override
  Widget build(BuildContext context, int state, AppActions actions) {
    return Home(
      counter: state,
      onIncrementClick: () {
        actions.increment();
      },
    );
  }

  @override
  int connect(AppState state) {
    return state.counter;
  }
}
