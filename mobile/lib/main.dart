import 'package:built_redux/built_redux.dart';
import 'package:common/common.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'package:mobile/di/di.dart';
import 'package:mobile/presentation/containers/home.dart';

void main() async {
  await AppInjector().initialize();

  runApp(new DummyApp());
}

class DummyApp extends StatefulWidget {
  final store = Store<AppState, AppStateBuilder, AppActions>(
    appReducerBuilder.build(),
    AppState(),
    AppActions(),
    middleware: [
      createAppMiddleware(AppInjector().appRepository()),
    ],
  );

  @override
  _DummyAppState createState() => _DummyAppState();
}

class _DummyAppState extends State<DummyApp> {
  Store<AppState, AppStateBuilder, AppActions> store;

  @override
  void initState() {
    store = widget.store;
//    store.actions.loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ReduxProvider(
      store: store,
      child: new MaterialApp(
        title: 'Dummy App',
        theme: new ThemeData(
          primarySwatch: Colors.deepOrange,
        ),
        routes: {
          AppRoutes.home: (context) => HomePage(),
        },
      ),
    );
  }
}
