import 'package:mobile/data/entities/entities.dart';

abstract class MapperFactory {
  DummyMapper dummyMapper();
}

class AppMapperFactory implements MapperFactory {
  @override
  DummyMapper dummyMapper() {
    return DummyMapper();
  }
}
