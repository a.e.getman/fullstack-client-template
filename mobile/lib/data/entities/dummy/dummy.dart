library dummy;

export 'bean.dart';
export 'entity.dart';
export 'mapper.dart';
export 'serializer.dart';
