// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializer.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$DummyEntitySerializer implements Serializer<DummyEntity> {
  @override
  Map<String, dynamic> toMap(DummyEntity model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'id', model.id);
    setMapValue(ret, 'name', model.name);
    return ret;
  }

  @override
  DummyEntity fromMap(Map map) {
    if (map == null) return null;
    final obj = new DummyEntity();
    obj.id = map['id'] as int;
    obj.name = map['name'] as String;
    return obj;
  }
}
