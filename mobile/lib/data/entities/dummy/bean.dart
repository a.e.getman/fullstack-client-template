import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:mobile/data/entities/dummy/entity.dart';

part 'bean.jorm.dart';

@GenBean()
class DummyBean extends Bean<DummyEntity> with _DummyBean {
  DummyBean(Adapter adapter) : super(adapter);

  @override
  String get tableName => "_dummy";
}
