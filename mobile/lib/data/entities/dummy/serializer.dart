import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:mobile/data/entities/dummy/entity.dart';

part 'serializer.jser.dart';

@GenSerializer()
class DummyEntitySerializer extends Serializer<DummyEntity>
    with _$DummyEntitySerializer {}
