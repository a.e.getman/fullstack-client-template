import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';

class DummyEntity {
  @Alias("id")
  @PrimaryKey()
  int id;

  @Alias("name")
  @Column()
  String name;

  DummyEntity();

  DummyEntity.all({
    this.id,
    this.name,
  });

  @override
  String toString() {
    return 'DummyEntity{id: $id, name: $name}';
  }
}
