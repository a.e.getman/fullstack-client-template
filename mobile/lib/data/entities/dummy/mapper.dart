import 'package:common/models/models.dart';

import 'entity.dart';

class DummyMapper {
  Dummy fromEntity(DummyEntity entity) {
    return Dummy.builder(
      (b) => b
        ..id = entity.id
        ..name = entity.name,
    );
  }

  DummyEntity toEntity(Dummy item) {
    return DummyEntity.all(
      id: item.id,
      name: item.name,
    );
  }
}
