import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:mobile/data/entities/entities.dart';

// todo: create code generator tool to remove boilerplate
abstract class BeanFactory {
  DummyBean dummyBean();
}

class AppBeanFactory implements BeanFactory {
  final SqfliteAdapter _adapter;
  DummyBean _dummyBean;

  AppBeanFactory(this._adapter) {
    _init();
  }

  void _init() async {
    await _adapter.connect();

    await dummyBean().createTable(ifNotExists: true);
  }

  @override
  DummyBean dummyBean() {
    if (_dummyBean == null) {
      _dummyBean = DummyBean(_adapter);
    }
    return _dummyBean;
  }
}
