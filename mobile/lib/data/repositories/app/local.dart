import 'package:mobile/data/db/db.dart';
import 'package:mobile/data/entities/entities.dart';

abstract class AppLocalDataSource {
  Future<List<DummyEntity>> loadData();
}

class RealAppLocalDataSource implements AppLocalDataSource {
  final BeanFactory beanFactory;

  const RealAppLocalDataSource({this.beanFactory});

  @override
  Future<List<DummyEntity>> loadData() async {
    final result = await beanFactory.dummyBean().getAll();
    return result;
  }
}
