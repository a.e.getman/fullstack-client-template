import 'package:common/data/data.dart';
import 'package:common/models/dummy/dummy.dart';
import 'package:mobile/data/entities/entities.dart';
import 'package:mobile/data/repositories/app/app.dart';

class FlutterAppRepository implements AppRepository {
  final AppLocalDataSource localDataSource;
  final AppRemoteDataSource remoteDataSource;
  final MapperFactory mapperFactory;

  const FlutterAppRepository({
    this.localDataSource,
    this.remoteDataSource,
    this.mapperFactory,
  });

  @override
  Future<List<Dummy>> loadData() async {
    try {
      return await localDataSource.loadData().then((dataList) => dataList
          .map((entity) => mapperFactory.dummyMapper().fromEntity(entity))
          .toList());
    } catch (e) {
      return await remoteDataSource.loadData().then((dataList) => dataList
          .map((entity) => mapperFactory.dummyMapper().fromEntity(entity))
          .toList());
    }
  }
}
