import 'package:mobile/data/entities/entities.dart';
import 'package:mobile/data/network/network.dart';

abstract class AppRemoteDataSource {
  Future<List<DummyEntity>> loadData();
}

class RealAppRemoteDataSource implements AppRemoteDataSource {
  final DummyApi api;

  const RealAppRemoteDataSource({this.api});

  @override
  Future<List<DummyEntity>> loadData() {
    return api.getData();
  }
}
