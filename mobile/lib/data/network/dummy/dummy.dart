import 'package:jaguar_retrofit/jaguar_retrofit.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';
import 'package:mobile/data/entities/entities.dart';

part 'dummy.jretro.dart';

@GenApiClient()
class DummyApi extends _$DummyApiClient implements ApiClient {
  final Route baseRoute;
  final SerializerRepo baseSerializer;

  DummyApi({this.baseRoute, this.baseSerializer});

  @override
  Route get base => baseRoute;

  @override
  SerializerRepo get serializers => baseSerializer;

  @GetReq()
  Future<List<DummyEntity>> getData();
}
