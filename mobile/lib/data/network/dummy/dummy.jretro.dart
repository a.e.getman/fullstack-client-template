// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dummy.dart';

// **************************************************************************
// JaguarHttpGenerator
// **************************************************************************

abstract class _$DummyApiClient implements ApiClient {
  final String basePath = "";
  Future<List<dynamic>> getData() async {
    var req = base.get.path(basePath);
    return serializers.from(await req.go().body);
  }
}
