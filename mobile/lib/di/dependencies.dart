import 'package:common/data/repositories/repositories.dart';
import 'package:mobile/data/data.dart';

abstract class AppDependencies {
  BeanFactory beanFactory();

  AppRepository appRepository();
}
