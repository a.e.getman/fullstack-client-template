import 'package:common/common.dart';
import 'package:common/data/repositories/app.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:http/http.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:jaguar_retrofit/jaguar_retrofit.dart';
import 'package:mobile/data/data.dart';
import 'package:mobile/di/dependencies.dart';
import 'package:mobile/utils/utils.dart';

class AppInjector implements AppDependencies {
  static final AppInjector _instance = AppInjector._internal();

  factory AppInjector() {
    return _instance;
  }

  AppInjector._internal();

  final injector = Injector.getInjector();

  Future initialize() async {
    await _initDbItems();
    _initNetworkItems();
    _initRepositoryItems();
  }

  Future _initDbItems() async {
    await FlutterAppInfo()
        .getDbPath(FlutterAppInfo.db_name)
        .then((path) => injector.map<SqfliteAdapter>(
              (i) => SqfliteAdapter(path, version: FlutterAppInfo.db_version),
              isSingleton: true,
            ));
    injector.map<BeanFactory>(
      (i) => AppBeanFactory(i.get<SqfliteAdapter>()),
      isSingleton: true,
    );
  }

  void _initNetworkItems() {
    injector.map<AppSerializer>((i) => AppSerializer());

    globalClient = IOClient();
    injector.map<DummyApi>((i) => DummyApi(
          baseRoute: route(ApiInfo.api_base_url),
          baseSerializer: i.get<AppSerializer>().todosRepo(),
        ));
  }

  void _initRepositoryItems() {
    injector.map<AppLocalDataSource>(
      (i) => RealAppLocalDataSource(beanFactory: i.get<BeanFactory>()),
    );
    injector.map<AppRemoteDataSource>(
      (i) => RealAppRemoteDataSource(api: i.get<DummyApi>()),
    );
    injector.map<AppRepository>(
      (i) => FlutterAppRepository(
            localDataSource: i.get<AppLocalDataSource>(),
            remoteDataSource: i.get<AppRemoteDataSource>(),
          ),
    );
  }

  @override
  BeanFactory beanFactory() {
    return injector.get<BeanFactory>();
  }

  @override
  AppRepository appRepository() {
    return injector.get<AppRepository>();
  }
}
