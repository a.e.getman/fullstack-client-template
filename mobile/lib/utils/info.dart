import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FlutterAppInfo {
  static const db_version = 1;
  static const db_name = "dummy";

  Future<String> getDbPath(String name) async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + name + ".db";
    return path;
  }
}
